﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestService.Data.Base
{
    class TestConnectionStrings
    {
        public static string AgencyManagementConnectionString { get { return ConfigurationManager.ConnectionStrings["AgencyManagementConnectionString"].Name; } }
        public static string AxxessLookupConnectionString { get { return ConfigurationManager.ConnectionStrings["AxxessLookupConnectionString"].Name; } }
        public static string OasisCConnectionString { get { return ConfigurationManager.ConnectionStrings["OasisCConnectionString"].Name; } }
    }
}
