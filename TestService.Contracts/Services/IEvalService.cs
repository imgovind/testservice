﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TestService.Contracts.Data;

namespace TestService.Contracts.Services
{
    [ServiceContract]
    public interface IEvalService
    {
        [OperationContract]
        bool SubmitEval(Eval eval);
        [OperationContract]
        List<Eval> GetEvals();
        [OperationContract]
        bool DeleteEvals();
    }
}
