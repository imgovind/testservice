﻿namespace TestService.Framework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using TestService.Framework.Extensibility;

    public static class Framework
    {
        public static void Initialize()
        {
            Container.InitializeWith(new MunqTypeResolver());
        }
    }
}
