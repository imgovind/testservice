﻿namespace TestService.Host
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using System.Threading.Tasks;
    using TestService.Contracts.Data;
    using TestService.Contracts.Services;
    using TestService.Data;
    using TestService.Data.Base;
    using TestService.Framework.DataAccess;
    using TestService.Framework.Extensibility;
    using TestService.Host.Base;
    using TestService.Services;

    class Program
    {
        static void Main(string[] args)
        {
            TestService.Framework.Framework.Initialize();
            Container.Register<IDataProvider, DataProvider>(ObjectLifeSpans.Singleton);
            Container.Register<IAgencyService, AgencyService>(ObjectLifeSpans.Singleton);
            Container.Register<IEvalService, EvalService>(ObjectLifeSpans.Singleton);
            PropertyCache.Register(typeof(Agency));
            Console.WriteLine("Starting Eval Service");

            //ServiceHost host = new ServiceHost(typeof(EvalService));

            //MunqServiceHost host = new MunqServiceHost(new ChildContainer(new MunqTypeResolver()), typeof(EvalService));

            MunqServiceHost host = new MunqServiceHost(typeof(AgencyService));
            try
            {
                host.Open();
                PrintServiceInfo(host);
                Console.ReadLine();
                host.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                host.Abort();
            }
        }

        static void PrintServiceInfo(ServiceHost host)
        {
            Console.WriteLine(string.Format("{0} is Running with the following endpoints", host.Description.Name));
            foreach (var item in host.Description.Endpoints)
            {
                Console.WriteLine("{0}", item.Address);
            }
        }
    }
}
