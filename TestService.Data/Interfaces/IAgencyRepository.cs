﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestService.Contracts.Data;

namespace TestService.Data.Interfaces
{
    public interface IAgencyRepository
    {
        List<Agency> GetAgencies();
        Agency GetAgency(Guid Id);
    }
}
