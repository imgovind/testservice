﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TestService.Contracts.Data
{
    [DataContract]
    public class Eval
    {
        #region Constructors
        public Eval()
        {

        }

        public Eval(string submitter, string comments)
        {
            this.Submitter = submitter;
            this.Comments = comments;
            this.Timesent = DateTime.Now;
        } 
        #endregion

        [DataMember]
        public string Submitter { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public DateTime Timesent { get; set; }
    }
}
