﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TestService.Client.Base;
using TestService.Contracts.Services;

namespace TestService.Client.Clients
{
    public static class AgencyClient
    {
        public static void Process()
        {
            ChannelFactory<IAgencyService> factory = new ChannelFactory<IAgencyService>(AppAgency.TCPService);

            IAgencyService channel = factory.CreateChannel();

            try
            {
                var result = channel.GetAllAgencies();
                ((IClientChannel)channel).Close();
                Console.WriteLine(result.Count);
                Console.WriteLine("");
            }
            catch (FaultException fe)
            {
                Console.WriteLine("FaultException Handler => " + fe.GetType());
                ((IClientChannel)channel).Abort();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("CommunicationException Handler => " + ce.GetType());
                ((IClientChannel)channel).Abort();
            }
            catch (TimeoutException te)
            {
                Console.WriteLine("TimeoutException Handler => " + te.GetType());
                ((IClientChannel)channel).Abort();
            }
        }
    }
}
