﻿namespace TestService.Host.Extensibility
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel.Dispatcher;
    using System.Text;
    using System.Threading.Tasks;
    using TestService.Framework.Extensibility;

    public class MunqInstanceProvider : IInstanceProvider
    {
        private readonly IChildContainer _container;
        private readonly Type _contractType;

        public MunqInstanceProvider(IChildContainer container, Type contractType)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }
            if (contractType == null)
            {
                throw new ArgumentNullException("contractType");
            }
            _container = container;
            _contractType = contractType;
        }

        #region IInstanceProvider Members

        public object GetInstance(System.ServiceModel.InstanceContext instanceContext, System.ServiceModel.Channels.Message message)
        {
            var childContainer = instanceContext.Extensions.Find<MunqInstanceContextExtension>().GetChildContainer(_container);
            return childContainer.Resolve(_contractType);
        }

        public object GetInstance(System.ServiceModel.InstanceContext instanceContext)
        {
            return GetInstance(instanceContext, null);
        }

        public void ReleaseInstance(System.ServiceModel.InstanceContext instanceContext, object instance)
        {
            instanceContext.Extensions.Find<MunqInstanceContextExtension>().DisposeOfChildContainer();
        }

        #endregion
    }
}
