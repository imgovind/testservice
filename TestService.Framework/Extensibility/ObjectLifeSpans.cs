﻿namespace TestService.Framework.Extensibility
{
    using System;

    public enum ObjectLifeSpans
    {
        Singleton = 1,
        Transient = 2,
        Request = 3,
        Thread = 4,
        Session = 5,
        Cached = 6
    }
}
