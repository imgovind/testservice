﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestService.Contracts.Data;
using TestService.Contracts.Services;
using TestService.Data;

namespace TestService.Services
{
    public class AgencyService : IAgencyService
    {
        #region Private Members and Constructors
        private IDataProvider _DataProvider;

        public AgencyService(IDataProvider dataProvider)
        {
            _DataProvider = dataProvider;
        }
        #endregion
        
        #region IAgencyService Members

        public List<Agency> GetAllAgencies()
        {
            var result = new List<Agency>();
            result = _DataProvider.AgencyRepository.GetAgencies();
            return result;
        }

        public Agency GetAgency(Guid Id)
        {
            var result = new Agency();
            result = _DataProvider.AgencyRepository.GetAgency(Id);
            return result;
        }

        #endregion
    }
}
