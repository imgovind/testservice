﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestService.Client.Base
{
    public static class AppEval
    {
        public static string BasicHTTPService { get { return "basicHttpEvalService"; } }
        public static string WSHTTPService { get { return "wsHttpEvalService"; } }
        public static string TCPService { get { return "tcpEvalService"; } }
        public static string NamedPipeService { get { return "pipeEvalService"; } }
    }

    public static class AppAgency
    {
        public static string BasicHTTPService { get { return "basicHttpAgencyService"; } }
        public static string WSHTTPService { get { return "wsHttpAgencyService"; } }
        public static string TCPService { get { return "tcpAgencyService"; } }
        public static string NamedPipeService { get { return "pipeAgencyService"; } }
    }
}
