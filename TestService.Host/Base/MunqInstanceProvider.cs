﻿namespace TestService.Host.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel.Dispatcher;
    using System.Text;
    using System.Threading.Tasks;
    using TestService.Framework.Extensibility;

    public class MunqInstanceProvider : IInstanceProvider
    {
        private readonly Type _contractType;

        public MunqInstanceProvider(Type contractType)
        {
            if (contractType == null)
            {
                throw new ArgumentNullException("contractType");
            }
            _contractType = contractType;
        }

        #region IInstanceProvider Members

        public object GetInstance(System.ServiceModel.InstanceContext instanceContext, System.ServiceModel.Channels.Message message)
        {
            return Container.Resolve(_contractType);
        }

        public object GetInstance(System.ServiceModel.InstanceContext instanceContext)
        {
            return GetInstance(instanceContext, null);
        }

        public void ReleaseInstance(System.ServiceModel.InstanceContext instanceContext, object instance)
        {
        }

        #endregion
    }
}
