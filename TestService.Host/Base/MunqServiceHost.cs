﻿namespace TestService.Host.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using System.Text;
    using System.Threading.Tasks;
    using TestService.Framework.Extensibility;

    public class MunqServiceHost : ServiceHost
    {
        public MunqServiceHost(Type serviceType, params Uri[] baseAddresses) 
            : base(serviceType, baseAddresses)
        {

            ApplyContractBehaviors();

            ApplyServiceBehaviors();

            foreach (var contractDescription in ImplementedContracts.Values)
            {
                var contractBehavior = new MunqContractBehavior(new MunqInstanceProvider(contractDescription.ContractType));
                contractDescription.ContractBehaviors.Add(contractBehavior);
            }
        }

        private void ApplyContractBehaviors()
        {
            //Register IContractBehavior in IChildContainer
            var registeredContractBehaviors = Container.ResolveAll<IContractBehavior>();
            foreach (var contractBehavior in registeredContractBehaviors)
            {
                foreach (var contractDescription in ImplementedContracts.Values)
                {
                    contractDescription.ContractBehaviors.Add(contractBehavior);
                }
            }
        }

        private void ApplyServiceBehaviors()
        {
            //Register IServiceBehavior in IChildContainer
            var registeredServiceBehaviors = Container.ResolveAll<IServiceBehavior>();
            foreach (var serviceBehavior in registeredServiceBehaviors)
            {
                Description.Behaviors.Add(serviceBehavior);
            }
        }
    }
}
