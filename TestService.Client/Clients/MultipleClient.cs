﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestService.Client.Base;
using System.ServiceModel;
using TestService.Contracts.Services;
using TestService.Contracts.Data;

namespace TestService.Client.Clients
{
    public static class MultipleClient
    {
        public static void Process()
        {
            List<string> endPointConfigurationNames = new List<string>() { AppEval.BasicHTTPService, AppEval.WSHTTPService, AppEval.TCPService, AppEval.NamedPipeService };

            foreach (var endPointConfigurationName in endPointConfigurationNames)
            {
                ChannelFactory<IEvalService> factory = new ChannelFactory<IEvalService>(endPointConfigurationName);
                Console.WriteLine("Eval benchmark -> {0}", endPointConfigurationName);
                EvalBenchmark(factory, endPointConfigurationName);
            }
        }

        private static void EvalBenchmark(ChannelFactory<IEvalService> factory, string endPointConfigurationName)
        {
            IEvalService channel = factory.CreateChannel();
            try
            {
                channel.DeleteEvals();
                SubmitEval(channel, 100);
                GetEvals(channel);
                ((IClientChannel)channel).Close();
                Console.WriteLine("");
            }
            catch (FaultException fe)
            {
                Console.WriteLine("FaultException Handler => " + fe.GetType());
                ((IClientChannel)channel).Abort();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("CommunicationException Handler => " + ce.GetType());
                ((IClientChannel)channel).Abort();
            }
            catch (TimeoutException te)
            {
                Console.WriteLine("TimeoutException Handler => " + te.GetType());
                ((IClientChannel)channel).Abort();
            }
        }

        private static void GetEvals(IEvalService channel)
        {
            DateTime startTimer = DateTime.Now; ;
            var evals = channel.GetEvals();
            TimeSpan endTimer = DateTime.Now - startTimer;
            Console.WriteLine("Total Retrieved => {0}",evals.Count);
            Console.WriteLine("Receiving: {0}", endTimer.TotalSeconds);
        }

        private static void SubmitEval(IEvalService channel)
        {
            DateTime startTimer = DateTime.Now;
            channel.SubmitEval(new Eval("I'll be your gift", "Katy Perry"));
            TimeSpan endTimer = DateTime.Now - startTimer;
            Console.WriteLine("Sending: {0}", endTimer.TotalSeconds);
        }

        private static void SubmitEval(IEvalService channel, int recordsCount)
        {
            DateTime startTimer = DateTime.Now;
            for (int i = 0; i < recordsCount; i++)
            {
                channel.SubmitEval(new Eval("I'll be your gift", "Katy Perry"));
                if (i % (recordsCount / 10) == 0)
                {
                    Console.Write("-");
                }
            }
            TimeSpan endTimer = DateTime.Now - startTimer;
            Console.WriteLine("");
            Console.WriteLine("Sending: {0}", endTimer.TotalSeconds);
        }
    }
}
