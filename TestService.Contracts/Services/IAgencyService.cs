﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TestService.Contracts.Data;

namespace TestService.Contracts.Services
{
    [ServiceContract]
    public interface IAgencyService
    {
        [OperationContract]
        List<Agency> GetAllAgencies();
        [OperationContract]
        Agency GetAgency(Guid Id);
    }
}
