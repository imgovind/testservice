﻿namespace TestService.Host.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using System.Text;
    using System.Threading.Tasks;
    using TestService.Framework.Extensibility;

    public abstract class MunqServiceHostFactory : ServiceHostFactory
    {
        protected abstract void ConfigureContainer(IChildContainer container);

        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            var container = Container.CreateChildContainer();
            ConfigureContainer(container);
            return new MunqServiceHost(serviceType, baseAddresses);
        }
    }
}
