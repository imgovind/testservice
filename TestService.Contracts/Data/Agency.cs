﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TestService.Contracts.Data
{
    [DataContract]
    public class Agency
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid Trainer { get; set; }
        [DataMember]
        public Guid BackupTrainer { get; set; }
        [DataMember]
        public Guid SalesPerson { get; set; }
        [DataMember]
        public Guid ImplementationSpecialist { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string TaxId { get; set; }
        [DataMember]
        public string TaxIdType { get; set; }
        [DataMember]
        public string Payor { get; set; }
        [DataMember]
        public string NationalProviderNumber { get; set; }
        [DataMember]
        public string MedicareProviderNumber { get; set; }
        [DataMember]
        public string MedicaidProviderNumber { get; set; }
        [DataMember]
        public string HomeHealthAgencyId { get; set; }
        [DataMember]
        public string SubmitterId { get; set; }
        [DataMember]
        public string SubmitterName { get; set; }
        [DataMember]
        public string SubmitterPhone { get; set; }
        [DataMember]
        public string SubmitterFax { get; set; }
        [DataMember]
        public bool IsAgreementSigned { get; set; }
        [DataMember]
        public bool IsSuspended { get; set; }
        [DataMember]
        public int TrialPeriod { get; set; }
        [DataMember]
        public int Package { get; set; }
        [DataMember]
        public int AnnualPlanId { get; set; }
        [DataMember]
        public string ContactPersonFirstName { get; set; }
        [DataMember]
        public string ContactPersonLastName { get; set; }
        [DataMember]
        public string ContactPersonEmail { get; set; }
        [DataMember]
        public string ContactPersonPhone { get; set; }
        [DataMember]
        public DateTime Modified { get; set; }
        [DataMember]
        public DateTime Created { get; set; }
        [DataMember]
        public bool IsDeprecated { get; set; }
        [DataMember]
        public int CahpsVendor { get; set; }
        [DataMember]
        public string CahpsVendorId { get; set; }
        [DataMember]
        public string CahpsSurveyDesignator { get; set; }
        [DataMember]
        public bool IsAxxessTheBiller { get; set; }
        [DataMember]
        public int OasisAuditVendor { get; set; }
        [DataMember]
        public string OasisAuditVendorApiKey { get; set; }
        [DataMember]
        public DateTime FrozenDate { get; set; }
        [DataMember]
        public bool IsFrozen { get; set; }
        [DataMember]
        public string AccountId { get; set; }
        [DataMember]
        public int ClusterId { get; set; }
    }
}
