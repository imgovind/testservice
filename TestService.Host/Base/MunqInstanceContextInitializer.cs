﻿namespace TestService.Host.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel.Dispatcher;
    using System.Text;
    using System.Threading.Tasks;

    public class MunqInstanceContextInitializer : IInstanceContextInitializer
    {
        #region IInstanceContextInitializer Members

        public void Initialize(System.ServiceModel.InstanceContext instanceContext, System.ServiceModel.Channels.Message message)
        {
            instanceContext.Extensions.Add(new MunqInstanceContextExtension());
        }

        #endregion
    }
}
