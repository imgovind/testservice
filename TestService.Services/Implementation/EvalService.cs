﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TestService.Contracts.Data;
using TestService.Contracts.Services;

namespace TestService.Services
{
    [ServiceBehavior(InstanceContextMode=InstanceContextMode.Single)]
    public class EvalService : IEvalService
    {
        #region Private Members
        List<Eval> evals = new List<Eval>(); 
        #endregion

        #region IEvalService Members

        public bool SubmitEval(Eval eval)
        {
            var result = false;
            var beforeCount = evals.Count;
            evals.Add(eval);
            var afterCount = evals.Count;
            if (afterCount > beforeCount) 
            {
                result = true;
            }
            return result;
        }

        public List<Eval> GetEvals()
        {
            return evals;
        }

        public bool DeleteEvals()
        {
            var result = false;
            evals = new List<Eval>();
            if (evals.Count == 0)
            {
                result = true;
            }
            return result;
        }
        #endregion
    }
}
