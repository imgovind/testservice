﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SubSonic.Repository;
using TestService.Contracts.Data;
using TestService.Data.Interfaces;
using TestService.Framework.DataAccess;
using TestService.Framework.Validation;

namespace TestService.Data.Implementations
{
    public class AgencyRepository : BaseRepository, IAgencyRepository
    {
        #region Private Members

        private readonly SimpleRepository _database;

        #endregion

        #region Constructor

        public AgencyRepository(string connectionName) : base(connectionName) { }

        public AgencyRepository(string connectionName, SimpleRepository database) : base(connectionName) { Check.Argument.IsNotNull(database, "database"); _database = database; }

        #endregion

        #region IAgencyRepository Members

        public List<Agency> GetAgencies()
        {
            var result1 = _database.All<Agency>().ToList();
            var result2 = this.Database.Select<Agency>(@"SELECT * FROM agencies WHERE IsDeprecated = 0;").ToList();
            var result3 = this.Database.Select<Agency>(x => x.IsDeprecated == false);
            return result2;
        }

        public Agency GetAgency(Guid Id)
        {
            var result = this.Database.Get<Agency>(string.Format(@"SELECT * FROM agencies WHERE Id = '{0}' AND agencies.IsDeprecated = 0;", Id));
            return result;
        }
        #endregion
    }
}
