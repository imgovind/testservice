﻿namespace TestService.Host.Extensibility
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using System.Threading.Tasks;
    using TestService.Framework.Extensibility;

    public class MunqInstanceContextExtension : IExtension<InstanceContext>
    {
        private IChildContainer _childContainer;

        public void Attach(InstanceContext owner)
        { }

        public void Detach(InstanceContext owner)
        { }

        public void DisposeOfChildContainer()
        {
            if (_childContainer != null)
            {
                _childContainer.Dispose();
            }
        }

        public IChildContainer GetChildContainer(IChildContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }
            Contract.EndContractBlock();
            return _childContainer ?? (_childContainer = container.CreateChildContainer());
        }
    }
}
