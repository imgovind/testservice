﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using TestService.Contracts.Data;
using TestService.Contracts.Services;

namespace TestService.Client.Clients
{
    public static class ChannelClient
    {
        public static void Process()
        {
            ServiceEndpointCollection serviceEndpoints = MetadataResolver.Resolve(typeof(IEvalService), new EndpointAddress("http://localhost:9090/evals/mex"));
            MetadataExchangeClient exchangeClient = new MetadataExchangeClient("http://localhost:9090/evals/mex");
            foreach (var serviceEndpoint in serviceEndpoints)
            {
                Console.WriteLine("Eval benchmark -> {0}", serviceEndpoint.Address);
                ChannelFactory<IEvalService> factory = new ChannelFactory<IEvalService>(serviceEndpoint.Binding, serviceEndpoint.Address);
                EvalBenchmark(factory, serviceEndpoint);    
            }
        }

        private static void EvalBenchmark(ChannelFactory<IEvalService> factory, ServiceEndpoint serviceEndpoint)
        {
            IEvalService channel = factory.CreateChannel();
            try
            {
                SubmitEval(channel, 1000);
                if (!serviceEndpoint.Name.ToLower().Contains("http"))
                {
                    GetEvals(channel);
                }
                ((IClientChannel)channel).Close();
            }
            catch (FaultException fe)
            {
                Console.WriteLine("FaultException Handler => " + fe.GetType());
                ((IClientChannel)channel).Abort();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("CommunicationException Handler => " + ce.GetType());
                ((IClientChannel)channel).Abort();
            }
            catch (TimeoutException te)
            {
                Console.WriteLine("TimeoutException Handler => " + te.GetType());
                ((IClientChannel)channel).Abort();
            }
        }

        private static void GetEvals(IEvalService channel)
        {
            DateTime startTimer = DateTime.Now;;
            channel.GetEvals();
            TimeSpan endTimer = DateTime.Now - startTimer;
            Console.WriteLine("Receiving: {0}", endTimer.TotalSeconds);
        }

        private static void SubmitEval(IEvalService channel)
        {
            DateTime startTimer = DateTime.Now; 
            channel.SubmitEval(new Eval("I'll be your gift", "Katy Perry"));
            TimeSpan endTimer = DateTime.Now - startTimer;
            Console.WriteLine("Sending: {0}", endTimer.TotalSeconds);
        }

        private static void SubmitEval(IEvalService channel, int recordsCount)
        {
            DateTime startTimer = DateTime.Now;
            for (int i = 0; i < recordsCount; i++)
            {
                channel.SubmitEval(new Eval("I'll be your gift", "Katy Perry"));
                if (i % (recordsCount / 10) == 0)
                {
                    Console.Write("-");
                }
            }
            TimeSpan endTimer = DateTime.Now - startTimer;
            Console.WriteLine("");
            Console.WriteLine("Sending: {0}", endTimer.TotalSeconds);
        }
    }
}
