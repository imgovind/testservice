﻿namespace TestService.Framework.Extensibility
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using LightInject;

    public class LightInjectTypeResolver : ITypeResolver
    {
        private readonly static ServiceContainer container = new ServiceContainer();

        #region Register Methods
        public void Register<I, T>()
            where I : class
            where T : class, I
        {
            Register<I, T>(ObjectLifeSpans.Transient);
        }

        public void Register<I, T>(string name)
            where I : class
            where T : class, I
        {
            Register<I, T>(name, ObjectLifeSpans.Transient);
        }

        public void Register<I, T>(ObjectLifeSpans lifeSpan)
            where I : class
            where T : class, I
        {
            switch (lifeSpan)
            {
                case ObjectLifeSpans.Singleton:
                    container.Register<I, T>(new PerContainerLifetime());
                    break;
                case ObjectLifeSpans.Transient:
                    container.Register<I, T>();
                    break;
                case ObjectLifeSpans.Request:
                    container.Register<I, T>(new PerRequestLifeTime());
                    break;
                case ObjectLifeSpans.Thread:
                    container.Register<I, T>(new PerThreadLifetime());
                    break;
                case ObjectLifeSpans.Session:
                    container.Register<I, T>(new PerContainerLifetime());
                    break;
                case ObjectLifeSpans.Cached:
                    container.Register<I, T>();
                    break;
                default:
                    container.Register<I, T>();
                    break;
            }
        }

        public void Register<I, T>(string name, ObjectLifeSpans lifeSpan)
            where I : class
            where T : class, I
        {
            switch (lifeSpan)
            {
                case ObjectLifeSpans.Singleton:
                    container.Register<I, T>(name, new PerContainerLifetime());
                    break;
                case ObjectLifeSpans.Transient:
                    container.Register<I, T>(name);
                    break;
                case ObjectLifeSpans.Request:
                    container.Register<I, T>(name, new PerRequestLifeTime());
                    break;
                case ObjectLifeSpans.Thread:
                    container.Register<I, T>(name, new PerThreadLifetime());
                    break;
                case ObjectLifeSpans.Session:
                    container.Register<I, T>(name, new PerContainerLifetime());
                    break;
                case ObjectLifeSpans.Cached:
                    container.Register<I, T>(name);
                    break;
                default:
                    container.Register<I, T>(name);
                    break;
            }
        }
        #endregion

        #region Inject Methods
        public void Inject<T>(T existing) where T : class
        {
            container.RegisterInstance<T>(existing);
        }

        public void Inject<T>(string name, T existing) where T : class
        {
            container.RegisterInstance<T>(existing, name);
        }

        public void Inject<T>(string name, T existing, ObjectLifeSpans lifeSpan) where T : class
        {
            switch (lifeSpan)
            {
                case ObjectLifeSpans.Singleton:
                    container.RegisterInstance<T>(existing, name);
                    break;
                case ObjectLifeSpans.Transient:
                    container.RegisterInstance<T>(existing, name);
                    break;
                case ObjectLifeSpans.Request:
                    container.RegisterInstance<T>(existing, name);
                    break;
                case ObjectLifeSpans.Thread:
                    container.RegisterInstance<T>(existing, name);
                    break;
                case ObjectLifeSpans.Session:
                    container.RegisterInstance<T>(existing, name);
                    break;
                case ObjectLifeSpans.Cached:
                    container.RegisterInstance<T>(existing, name);
                    break;
                default:
                    container.RegisterInstance<T>(existing, name);
                    break;
            }
        }
        #endregion
        
        #region Resolve Methods
        public object Resolve(Type type)
        {
            return container.GetInstance(type);
        }

        public T Resolve<T>() where T : class
        {
            return container.GetInstance<T>();
        }

        public T Resolve<T>(Type type) where T : class
        {
            return container.GetInstance<T>();
        }

        public T Resolve<T>(string name) where T : class
        {
            return container.GetInstance<T>(name);
        }

        public T Resolve<T>(Type type, string name) where T : class
        {
            return container.GetInstance<T>(name);
        }

        public IEnumerable<T> ResolveAll<T>() where T : class
        {
            return container.GetAllInstances<T>().ToList<T>();
        }

        public IEnumerable<T> ResolveAll<T>(Type type) where T : class
        {
            return container.GetAllInstances<T>().ToList<T>();
        } 
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
