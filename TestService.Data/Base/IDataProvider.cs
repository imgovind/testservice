﻿namespace TestService.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using TestService.Data.Interfaces;

    public interface IDataProvider
    {
        IAgencyRepository AgencyRepository
        {
            get;
        }
    }
}
