﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SubSonic.Repository;
using TestService.Data.Interfaces;

namespace TestService.Data.Base
{
    public class DataProvider : IDataProvider
    {
        #region Members and Properties

        private IAgencyRepository _AgencyRepository;

        private readonly SimpleRepository _database;

        public DataProvider()
        {
            _database = new SimpleRepository(TestConnectionStrings.AgencyManagementConnectionString, SimpleRepositoryOptions.None);
        }

        #endregion

        #region IDataProvider Members

        public Interfaces.IAgencyRepository AgencyRepository
        {
            get
            {
                if (_AgencyRepository == null)
                {
                    _AgencyRepository = RepositoryFactory.Instance.Get<IAgencyRepository>(new object[] { TestConnectionStrings.AgencyManagementConnectionString, _database });
                }
                return _AgencyRepository;
            }
        }

        #endregion
    }
}
