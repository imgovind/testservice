﻿namespace TestService.Host.Extensibility
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using System.Text;
    using System.Threading.Tasks;
    using TestService.Framework.Extensibility;

    public class MunqServiceHost : ServiceHost
    {
        public MunqServiceHost(IChildContainer container, Type serviceType, params Uri[] baseAddresses) 
            : base(serviceType, baseAddresses)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            ApplyContractBehaviors(container);

            ApplyServiceBehaviors(container);

            foreach (var contractDescription in ImplementedContracts.Values)
            {
                var contractBehavior = new MunqContractBehavior(new MunqInstanceProvider(container, contractDescription.ContractType));
                contractDescription.ContractBehaviors.Add(contractBehavior);
            }
        }

        private void ApplyContractBehaviors(IChildContainer container)
        {
            //Register IContractBehavior in IChildContainer
            var registeredContractBehaviors = container.ResolveAll<IContractBehavior>();
            foreach (var contractBehavior in registeredContractBehaviors)
            {
                foreach (var contractDescription in ImplementedContracts.Values)
                {
                    contractDescription.ContractBehaviors.Add(contractBehavior);
                }
            }
        }

        private void ApplyServiceBehaviors(IChildContainer container)
        {
            //Register IServiceBehavior in IChildContainer
            var registeredServiceBehaviors = container.ResolveAll<IServiceBehavior>();
            foreach (var serviceBehavior in registeredServiceBehaviors)
            {
                Description.Behaviors.Add(serviceBehavior);
            }
        }
    }
}
